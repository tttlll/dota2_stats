import matplotlib.pyplot as plt
import requests
import matplotlib.ticker as plticker


user_id = '105248644' #dota id (miracle)

fig, ax = plt.subplots()
loc = plticker.MultipleLocator(base=4.0)
ax.xaxis.set_major_locator(loc)
plt.rc('font', size=4)


url_user_match = 'https://api.opendota.com/api/players/{}/matches'.format(user_id)
r = requests.get(url_user_match).json()

mass=[0]*121
for match in r:
    mass[match.get('hero_id')] += 1

plt.bar(list(range(121)), mass)
plt.show()
