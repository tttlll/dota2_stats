import requests
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker

user_id = '306869263'
number_match = 1

'''Use proxy, because for RF block site'''

http_proxy = {'https':"http://65.49.1.33:60099"}
fig, ax = plt.subplots()

loc = plticker.MultipleLocator(base=1.0)
ax.xaxis.set_major_locator(loc)


def create_plot(names,damage):
    x_coord = list(range(1, 11))
    plt.rc('font', size=12)
    plt.xlabel('Count')
    plt.ylabel('Damage')
    rects = plt.bar(x_coord, damage, color='g', align='edge', width=0.40)

    autolabel(rects, names)
    plt.show()


def autolabel(rects, name):
    i = 0
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width() / 2., 1.02 * height,
                '%s' % name[i],
                ha='center', va='bottom')
        i += 1


def get_data(user_id, number_match):
    url_user_match = 'https://api.opendota.com/api/players/{}/matches'.format(user_id)
    r = requests.get(url_user_match, proxies=http_proxy)
    r = r.json()
    match_id = r[number_match].get('match_id')
    url_stat_match = 'https://api.opendota.com/api/matches/{}'.format(match_id)
    players = requests.get(url_stat_match, proxies=http_proxy).json().get('players')
    names = []
    damage = []
    for player in players:
        names.append('{}\n({})'.format(player.get('personaname'), player.get('hero_id')))
        damage.append(player.get('hero_damage'))
    return (names,damage,)


data=get_data(user_id=user_id,number_match=number_match)
create_plot(data[0], data[1])
